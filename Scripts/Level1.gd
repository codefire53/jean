extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
#onready var gameTimer = get_node("gameTimer")
# Called when the node enters the scene tree for the first time.
func _ready():
	global.playerHealth = global.playerMaxHealth # Replace with function body.
	global.potionCapacity = 0

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass#counterLabel.set_text(str(int(gameTimer.get_time_left())))


#func _on_gameTimer_timeout():
	#TODO: add game over


func _on_ToBossRoom_area_entered(area):
	if area.is_in_group("Player"):
		ScreenChanger.change_scene("res://Scenes/Cutscene1.tscn") # Replace with function body.

func game_over():
	get_tree().change_scene("res://Scenes/GameOver1.tscn")
	
func _on_Hero_player_dead():
	game_over()# Replace with function body.


func _on_Hero_start_timer():
	pass # Replace with function body.


func _on_Hero_stop_timer():
	pass # Replace with function body.
