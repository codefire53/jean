extends KinematicBody2D
export (int) var bulletRange = 40
export var LeftEnd = Vector2()
export var RightEnd = Vector2()
var canSpawnOrb = true
export (int) var hitDamage = 10
export (int) var healthPoint = 1000
var isAlive = true
var halfHealth = int(healthPoint/2)
var quarterHealth = int (healthPoint/2)
signal boss_death
func _ready():
	pass
	
func _process(_delta):
	if healthPoint <= 0:
		isAlive = false
	if isAlive:
		$AnimationPlayer.play("idle")
		if healthPoint > halfHealth:
			if canSpawnOrb:
				spawn_orbs()
		else:
			$AttackTimer.set_wait_time(5)
			if canSpawnOrb:
				spawn_magicBeam()
	else:
		$AnimationPlayer.play("dead")
	
func spawn_orbs():
	var initial_x = position.x - 90
	var initial_y = position.y + 40

	var OrbInst = load("res://Scenes/MagicalOrb.tscn")
	for _i in range(0,4):
		var orb = OrbInst.instance()	
		orb.position.x=initial_x
		orb.position.y=initial_y
		initial_y-=bulletRange
		$blasterAttack.play()
		get_node("../../BossStage").add_child(orb)
	for _j in range(0, 5):
		var orb = OrbInst.instance()
		orb.position.x=initial_x
		orb.position.y=initial_y
		initial_x +=bulletRange
		$blasterAttack.play()
		get_node("../../BossStage").add_child(orb)
	$AttackTimer.start()
	canSpawnOrb=false
func do_teleport():
	#visible=false
	randomize()
	var side = int(rand_range(0,2))
	if side==0:
		$Sprite.set_flip_h(false)
		position=LeftEnd
	else:
		$Sprite.set_flip_h(true)
		position=RightEnd
	#visible=true
func spawn_magicBeam():
	do_teleport()
	var MagicBeam = load("res://Scenes/MagicBeam.tscn")
	var Player = get_node("../Hero")
	var beam = MagicBeam.instance()
	if  position==RightEnd:
		beam.scale.x=1	
		beam.position.x=462
		beam.position.y=297
	else:
		beam.scale.x=-1
		beam.position.x=624
		beam.position.y=297
	get_node("../../BossStage").add_child(beam)
	$AttackTimer.start()
	canSpawnOrb=false
	
func _on_AttackTimer_timeout():
	 # Replace with function body.
	canSpawnOrb=true

func _on_Area2D_area_entered(area):
	if area.is_in_group("EnemyHit"):
		$hurtSound.play()
		healthPoint = max(healthPoint - hitDamage, 0)
		#print(healthPoint) # Replace with function body.


func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name=="dead":
		emit_signal("boss_death")
		queue_free() # Replace with function body.


func _on_AnimationPlayer_animation_started(anim_name):
	if anim_name=="dead":
		$deadSound.play() # Replace with function body.
