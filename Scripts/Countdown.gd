extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var Scene = get_tree().get_current_scene()

# Called when the node enters the scene tree for the first time.
func _ready():
	visible=false# Replace with function body.
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if get_node_or_null(("../../../../../gameTimer")):
		$Label.set_text("Time left: "+str(int(get_node(("../../../../../gameTimer")).time_left)))
		visible=true

