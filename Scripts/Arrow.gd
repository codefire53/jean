extends KinematicBody2D
export (float) var velocity_x = 10
export (float) var velocity_y = 10
var pos_player = 50
var time 
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var direction = Vector2()
# Called when the node enters the scene tree for the first time.
func _ready():
	time=velocity_x/pos_player # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _physics_process(delta):
	var collision
	if rotation_degrees < 0:
		direction.x = velocity_x*delta
		direction.y = velocity_y*delta+0.5*9.8*delta*delta
		rotation_degrees = int(min(float(rotation_degrees+0.0001),float(-60)))
		collision = move_and_collide(direction)
		
	else:
		direction.x = -velocity_x*delta
		direction.y = velocity_y*delta+0.5*9.8*delta*delta
		rotation_degrees = int(max(float(rotation_degrees-0.0001),float(30)))
		collision = move_and_collide(direction)
	if collision:
		$arrowHit.play()
		queue_free()



