extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$Camera2D.make_current() # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Polygon2D_cutscene_done():
	ScreenChanger.change_scene("res://Scenes/WinScreen.tscn")# Replace with function body.


func _on_LinkButton_pressed():
	ScreenChanger.change_scene("res://Scenes/WinScreen.tscn") # Replace with function body.
