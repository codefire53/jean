extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func game_over():
	get_tree().change_scene("res://Scenes/GameOver1.tscn")

func game_over2():
	get_tree().change_scene("res://Scenes/GameOver2.tscn")
func _on_Hero_player_dead():
	game_over()


func _on_Boss_boss_death():
	$gameTimer.stop()
	for node in get_tree().get_nodes_in_group("Entrance"):
		node.queue_free()


func _on_Hero_start_timer():
	$gameTimer.set_paused(false) # Replace with function body.



func _on_Hero_stop_timer():
	$gameTimer.set_paused(true) # Replace with function body.


func _on_gameTimer_timeout():
	game_over2() # Replace with function body.

