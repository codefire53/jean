extends CanvasLayer

onready var animationPlayer = $AnimationPlayer
onready var black = $Control/Black

func change_scene(path, delay = 0.5):
	yield(get_tree().create_timer(delay), "timeout")
	animationPlayer.play("fade")
	yield(animationPlayer, "animation_finished")
	animationPlayer.play_backwards("fade")
	get_tree().change_scene(path)

	#get_tree().change_scene()
