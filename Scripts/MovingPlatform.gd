extends KinematicBody2D
var direction = 1
export (int) var speed = 100
export (float) var changeTime = 4
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$Timer.set_wait_time(float(changeTime)) # Replace with function body.
	$Timer.start()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	position.x += (speed*delta*direction)


func _on_Timer_timeout():
	direction*=-1 # Replace with function body.
	$Timer.start()
