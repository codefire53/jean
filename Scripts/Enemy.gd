extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var movement = Vector2()
var up = Vector2(0,-1)
var speed = 30

#var speedJump = 500
#var isAttacking = false
var GRAVITY = 500
var direction
var right 
export (int) var hitDamage = 10
export (int) var healthPoint=20
onready var Player = get_node('../Hero')
# Called when the node enters the scene tree for the first time.
func _ready():
	direction = 1 # Replace with function body.
	$AnimatedSprite.play("walk")
	right = true

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):

func _physics_process(_delta):
	if healthPoint > 0:
		movement.x = speed*direction
		movement.y += GRAVITY
		
		if $lineOfSight.is_colliding() && $lineOfSight.get_collider()==Player:
			direction = 0
			$AnimatedSprite.play("attack")
			
		elif $backHit.is_colliding() && $backHit.get_collider()==Player:
			scale.x = -1
			direction = 0
			$AnimatedSprite.play("attack")
			right = !right
		elif $lineOfSight.is_colliding() || $lowerSight.is_colliding():
			direction *= -1
			scale.x *= -1
			right = !right
			
		elif right:
			direction = 1
			$AnimatedSprite.play("walk")
		else:
			direction = -1
			$AnimatedSprite.play("walk")
			
		if $rightBorder.is_colliding()==false:
			direction *= -1
			scale.x *= -1
			right = !right
		
		
		movement = move_and_slide(movement, up)	

		
func _process(delta):
	if healthPoint <= 0:
		$AnimatedSprite.play("dead")
	else:
		if $AnimatedSprite.animation == "attack" && $AnimatedSprite.frame==7:
			$axeSwing.play()
			$attackArea/CollisionShape2D.disabled=false
		else:
			$attackArea/CollisionShape2D.disabled=true
		
	




func _on_HitArea_area_entered(area):
	if area.is_in_group("EnemyHit") and healthPoint > 0:
		healthPoint = max(healthPoint-hitDamage,0) # Replace with function body.


func _on_AnimatedSprite_animation_finished():
	if $AnimatedSprite.animation == "dead":
		queue_free() # Replace with function body.
