extends KinematicBody2D

var direction = Vector2(-200,0) # Shoot to the right at 100px/sec
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var collisions = Vector2()

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _physics_process(delta): # Used to be _fixed_process(delta) in
	# Note, move_and_slide() should NOT multiply delta, that is managed internally as it accommodates sliding.
	# If this was a player, we'd be polling the Input to determine the move direction.
	collisions = move_and_collide(direction * delta) 
	if collisions:
		queue_free() # Look at what has been hit and behave appropriately, like dust fx for hitting a wall or damaging an enemy




func _on_Area2D_area_entered(area):
	if area.is_in_group("EnemyHit"):
		queue_free() # Replace with function body.
