extends ColorRect


export var dialogues = [
	"Darkbringer: Well...well..what do i have here?",
	"Jean: Release Eris, Now!",
	"Darkbringer: So you think that i would release that precious princess?",
	"Darkbringer: Hahahaha....No",
	"Darkbringer: I can absorb her power so that  I can become powerful and later bring up the chaos & darkness in the world huahaha",
	"Eris: AAAAAAh",
	"Jean: ERRIIIISS!",
	"Jean: Damn you darkbringer..with this holy sword i'll bring you to the justice for good",
	"Darkbringer: Very well servant of light, i'll put end to your misery & of course your precious princess altogether..huahahahaha"
,]
var page = 0
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
signal cutscene_done

# Called when the node enters the scene tree for the first time.
func _ready():
	$RichTextLabel.set_bbcode(dialogues[page]) # Replace with function body.
	$RichTextLabel.set_visible_characters(0)
	set_process_input(true)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _input(event):
	if event is InputEventMouseButton && event.is_pressed():
		if $RichTextLabel.get_visible_characters() > $RichTextLabel.get_total_character_count():
			if page < dialogues.size()-1:
				page += 1
				$RichTextLabel.set_bbcode(dialogues[page])
				$RichTextLabel.set_visible_characters(0)
			else:
				emit_signal("cutscene_done")

func _on_Timer_timeout():
	$RichTextLabel.set_visible_characters($RichTextLabel.get_visible_characters()+1) # Replace with function body.
