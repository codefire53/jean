extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export (float) var speed = 100
var direction = 1
onready var Player = get_node('../Hero')
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if $UpperRay.is_colliding() && !$UpperRay.get_collider()==Player:
		direction = 1
	elif $LowerRay.is_colliding():
		direction = -1
	position.y += (direction*speed*delta)

