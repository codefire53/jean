extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var isAttacking = false
var isAlive = true
onready var ArrowScene = load("res://Scenes/Arrow.tscn")
export (int) var health = 25
export (int) var hitDamage = 10
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if $flipPoint.is_colliding():
		scale.x*=-1
	if health <= 0:
		isAlive = false
	if isAlive:
		if isAttacking:
			$AnimatedSprite.play("attack")
		else:
			$AnimatedSprite.play("idle")
	else:
		$AnimatedSprite.play("dead")
	#print("yes")

func launch_arrow():
	print("yes")
	var ArrowInst = ArrowScene.instance()
	if scale.x > 0:
		ArrowInst.position.x = position.x + 10
		ArrowInst.position.y = position.y
		ArrowInst.rotation_degrees = -90
	else:
		ArrowInst.rotation_degrees = 90
		ArrowInst.position.x = position.x - 10
		ArrowInst.position.y = position.y
	$arrowRelease.play()
	get_node("../../Node2D").add_child(ArrowInst)

func _on_AnimatedSprite_frame_changed():
	if $AnimatedSprite.animation == "attack" && $AnimatedSprite.frame == 3:
		launch_arrow()
				
		

func _on_AnimatedSprite_animation_finished():
	if $AnimatedSprite.animation == "dead":
		queue_free()


func _on_FieldOfView_area_entered(area):
	if area.is_in_group("Player"):
		isAttacking = true # Replace with function body.


func _on_FieldOfView_area_exited(area):
	if area.is_in_group("Player"):
		isAttacking = false # Replace with function body.# Replace with function body.



func _on_HitBox_area_entered(area):
	if area.is_in_group("EnemyHit"):
		health = max(health-hitDamage, 0)

