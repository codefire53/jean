extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var direction = 1
var turning_degree = 90
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(_delta):
	if direction == 1:
		rotation_degrees = min(rotation_degrees+8,turning_degree)
		if rotation_degrees >= turning_degree:
			 direction = -1
			 turning_degree = -90
	else:
		rotation_degrees = max(rotation_degrees-8,turning_degree)
		if rotation_degrees <= turning_degree:
			direction = 1
			turning_degree = 90
	
