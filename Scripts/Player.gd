extends KinematicBody2D
var UP = Vector2(0,-1)
var velocity = Vector2()
var animTree
var animPlayer
var aniPlayer
var speed = 200
var jumpSpeed = 450
var direction = 1
export (int) var hitDamage = 10
export (int) var lifeAddition = 10
export (int) var lifeCost = 10
var isAlive = true
var gameOver = false
export (int) var GRAVITY = 980
var hasJumped = false
var special1CanUse = true
export (int) var currAttack = 0
export (int) var curAirAttack = 0
export (bool) var isAttacking = false 

signal player_dead
signal stop_timer
signal start_timer

func _ready():
	aniPlayer = self.get_node("AnimationPlayer")
	animTree=self.get_node("AnimationTree")
	animPlayer = animTree["parameters/playback"]
	animPlayer.start("idle")
func get_attack():
	if Input.is_action_just_pressed("Attack")  && (animPlayer.get_current_node()=="idle" || "attack" in animPlayer.get_current_node()):
		if !isAttacking:
			isAttacking=true
		$attackTimer.start()
		match currAttack:
			0:
				animPlayer.travel("attack1")
				currAttack+=1
			1:
				animPlayer.travel("attack2")
				currAttack+=1
			2:
				animPlayer.travel("attack3")
				currAttack=0
	elif Input.is_action_just_pressed("Attack")  && hasJumped:
		animPlayer.travel("airattackReady")
		$attackTimer.start()
		if Input.is_action_pressed("ui_down"):
			animPlayer.travel("airSlam")
			print("yes")
		else:
			match curAirAttack:
				0:
					animPlayer.travel("airAttack1")
					curAirAttack += 1
				1:
					animPlayer.travel("airAttack2")
					curAirAttack = 0
	elif Input.is_action_just_pressed("time_stop") && special1CanUse:
			$stopTimer.start()
			$special1Timer.start()
			special1CanUse = false
			setEnemies(false)
			emit_signal("stop_timer")
			global.playerHealth = max(global.playerHealth-lifeCost,0)
	
	if is_on_floor() && hasJumped:
		GRAVITY=980
		isAttacking=false
		hasJumped=false
		animPlayer.start("idle")
func get_input():

	if !isAttacking:
		if Input.is_action_just_pressed("jump") and is_on_floor():
			hasJumped=true
			velocity.y = -jumpSpeed
			animPlayer.travel("jump")
			
		elif is_on_floor():
			animPlayer.travel('idle')
			hasJumped = false
				
	if Input.is_action_pressed('ui_right'):
		direction = 1
		velocity.x = speed
		scale.x = 1 * scale.y
		if !hasJumped && !isAttacking:
			if Input.is_action_pressed("ui_down"):
				animPlayer.travel("slide")
			else:
				animPlayer.travel("run")
	
	elif Input.is_action_pressed("ui_left") :
		direction = -1
		velocity.x = -speed
		scale.x = -1 * scale.y
		if !hasJumped && !isAttacking:
			if Input.is_action_pressed("ui_down"):
				animPlayer.travel("slide")
			else:
				animPlayer.travel("run")
	else:
		velocity.x=0

func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().paused = true
		self.get_node("Camera2D").get_node("CanvasLayer").get_node("Control").get_node("PopupMenu").popup()
	elif Input.is_action_just_pressed("Heal"):
		if global.potionCapacity > 0:
			global.playerHealth = min(global.playerHealth+lifeAddition,global.playerMaxHealth)
			global.potionCapacity -= 1
	else:
		if global.playerHealth <= 0:
			isAlive = false
			animPlayer.stop()
			aniPlayer.play("dead")

		if isAlive:
			get_attack()
			get_input()
		velocity.y += delta*GRAVITY
		
		if isAlive:
			if velocity.y>=0 && hasJumped && !isAttacking:
				animPlayer.travel("fall")
			velocity=move_and_slide(velocity,UP)

func _on_HitArea_area_entered(area):
	if area.is_in_group("Threat") :
		global.playerHealth = max(global.playerHealth-hitDamage,0) # Replace with function body.
		if global.playerHealth > 0:
			$hurtSound.play()
	elif area.is_in_group("MagicBeam"):
		global.playerHealth=0
	
	elif area.is_in_group("lifeAddition"):
		global.potionCapacity += 1
	


func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name=="dead":
		aniPlayer.stop()
		emit_signal("player_dead")
		queue_free()
 # Replace with function body.


func _on_AnimationPlayer_animation_started(anim_name):
	if anim_name=="dead":
		pass # Replace with function body.

func _on_stopTimer_timeout():
	setEnemies(true) # Replace with function body.
	emit_signal("start_timer")

func _on_special1Timer_timeout():
	special1CanUse = true # Replace with function body.

func setEnemies(state):
	for node in get_tree().get_nodes_in_group("Enemy"):
		node.set_physics_process(state)
		node.set_process(state)
		if state:
			if node.get_node_or_null("AnimatedSprite"):
				node.get_node("AnimatedSprite").play()
			elif node.get_node_or_null("AnimationPlayer"):
				node.get_node("AnimationPlayer").play()
		else:
			if node.get_node_or_null("AnimatedSprite"):
				node.get_node("AnimatedSprite").stop()
			elif node.get_node_or_null("AnimationPlayer"):
				node.get_node("AnimationPlayer").stop()
		
	for node in get_tree().get_nodes_in_group("Particle"):
		node.set_physics_process(state)
		node.set_process(state)
	
	for node in get_tree().get_nodes_in_group("MovingStatic"):
		node.set_process(state)
	
	for node in get_tree().get_nodes_in_group("FallingSpike"):
		node.set_process(state)


func _on_attackTimer_timeout():
	isAttacking=false # Replace with function body.
