extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var Player = get_node("../Hero")
var isFalling
var movement = Vector2()
var up = Vector2(0,-1)
var GRAVITY = 2000
# Called when the node enters the scene tree for the first time.
func _ready():
	isFalling = false# Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if $RayCast2D.is_colliding() and $RayCast2D.get_collider()==Player:
		isFalling=true


func _physics_process(delta):
	if isFalling:
		print("yes")
		movement.y += delta * GRAVITY
		movement = move_and_slide(movement, up)
		if is_on_floor():
			queue_free()
